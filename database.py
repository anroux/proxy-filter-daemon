#!/usr/bin/env python3
import logging

logger = logging.getLogger(__name__)

import socket
import ipaddress
import redis

from itertools import islice
from metadatas import Domain, BannedClients

REDIS_URL = "unix:///var/run/redis/socket"


class Database(object):
    def __init__(self):
        self.r = redis.Redis(
            connection_pool=redis.BlockingConnectionPool.from_url(REDIS_URL)
        )

    def apply_tags_multiple(self, domains):
        logger.info("Applying {} domains to the database".format(len(domains)))

        logger.info("Fetching the database")
        db_domains = {x.name: x.tags for x in Domain.objects()}

        logger.info("Preparing the diff")
        to_update = {}
        to_insert = {}

        for domain, tags in domains.items():
            if domain in db_domains:
                if len(set(tags) - set(db_domains[domain])) > 0:
                    to_update[domain] = list(set(db_domains[domain]) | set(tags))
            else:
                to_insert[domain] = tags

        del db_domains

        if len(to_insert) > 0:
            logger.info("Bulk inserting {} entries".format(len(to_insert)))

            def chunks(data, n):
                it = iter(data)
                for i in range(0, len(data), n):
                    yield {k: data[k] for k in islice(it, n)}

            count = 0
            for chunk in chunks(to_insert, 5000):
                count += len(
                    Domain.objects.insert(
                        [Domain(name=name, tags=tags) for name, tags in chunk.items()],
                        load_bulk=False,
                    )
                )
                logger.info(
                    "{} entries inserted ({:.2f} %)".format(
                        count, count / len(to_insert) * 100
                    )
                )

        logger.info("Apply updates to {} entries".format(len(to_update)))
        count = 0
        for domain, tags in to_update.items():
            self.apply_tags(domain, tags, skip_cache=True)
            count += 1
            if count % 5000 == 0:
                logger.info(
                    "{} entries updated ({:.2f} %)".format(
                        count, count / len(to_update) * 100
                    )
                )

        logger.info("Flushing the cache")
        self.r.flushall()

        logger.info("Done")

    def clean_cache(self, domain):
        logger.info("Clean cache for {}".format(domain))

        keys = ["domain:" + domain] + list(
            map(lambda x: x.decode("utf-8"), self.r.keys("domain:*." + domain))
        )
        logger.info("Deleting [{}]".format(", ".join(keys)))

        for key in keys:
            self.r.delete(key)

    def apply_tags(self, domain, tags, skip_cache=False):
        db_domains = Domain.objects(name=domain)
        if len(db_domains) > 0:
            db_domains.update(add_to_set__tags=tags)
        else:
            db_domain = Domain(name=domain, tags=tags)
            db_domain.save()

        if not skip_cache:
            self.clean_cache(domain)

    def remove_tags(self, domain, tags, skip_cache=False):
        db_domains = Domain.objects(name=domain)
        db_domains.update(pull_all__tags=tags)

        if not skip_cache:
            self.clean_cache(domain)

    def set_comment(self, domain, comment):
        db_domains = Domain.objects(name=domain)
        if len(db_domains) > 0:
            db_domains.update(set__comment=comment)
        else:
            db_domain = Domain(name=domain, comment=comment)
            db_domain.save()

    def get_comment(self, domain):
        db_domain = Domain.objects(name=domain).first()
        if db_domain is not None:
            return db_domain.comment

        return None

    def get_domain_or_ip(self, domain_or_ip, skip_cache=False):
        if not skip_cache:
            cache = list(
                map(
                    lambda x: x.decode("utf-8"),
                    self.r.smembers("domain:" + domain_or_ip),
                )
            )
            if len(cache) > 0:
                return cache

        tags = set()
        try:
            # Test if the given string is an IP
            ipaddress.ip_address(domain_or_ip)
            ip = domain_or_ip

            tags.update(self.get_fulldomain(ip, skip_cache))

            # Do not reverse resolv the IP as many Cloudflare IP will SERVFAIL
            # ... and it's not so useful

        except ValueError:
            domain = domain_or_ip
            tags.update(self.get_parents(domain, skip_cache))

            try:
                ip = socket.gethostbyname(domain)

                ip_tags = self.get_fulldomain(ip, skip_cache)
                if len(ip_tags) > 0 and "ip-uncertain" not in ip_tags:
                    tags.update(ip_tags + ["ip-tagged"])

            except socket.gaierror:
                pass

        if len(tags) == 0:
            tags.add("unclassified")

        if not skip_cache:
            self.r.sadd("domain:" + domain_or_ip, *tags)
            self.r.expire("domain:" + domain_or_ip, 600)

        return list(tags)

    def get_fulldomain(self, domain, skip_cache=False):
        db_domain = Domain.objects(name=domain).first()
        if db_domain is not None and len(db_domain.tags) > 0:
            if not skip_cache:
                self.r.sadd("domain:" + domain, *set(db_domain.tags))
                self.r.expire("domain:" + domain, 600)

            return db_domain.tags

        return []

    def get_parents(self, domain, skip_cache=False):
        splitted_domain = domain.split(".")

        for i in range(len(splitted_domain), 1, -1):
            domain_part = ".".join(splitted_domain[-i:])
            tags = self.get_fulldomain(domain_part, skip_cache)
            if len(tags) > 0:
                return tags

        return []

    def is_client_banned(self, client_ip):
        key = "banned:" + client_ip

        banned_str = self.r.get(key)
        if banned_str is not None:
            return banned_str == b"True"

        db_banned_client = BannedClients.objects(ip=client_ip).first()
        banned = db_banned_client is not None

        with self.r.pipeline() as pipe:
            pipe.set(key, str(banned))
            pipe.expire(key, 120)
            pipe.execute()

        return banned
