#!/usr/bin/env python3
import os
import sentry_sdk

sentry_sdk.init(os.getenv("SENTRY_DSN"))

import logging
import logstash

logger = logging.getLogger()
logger.setLevel(logging.INFO)

logFormatter = logging.Formatter(
    "%(asctime)s [%(name)-30.30s] [%(levelname)-5.5s]  %(message)s"
)

consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(logFormatter)
logger.addHandler(consoleHandler)

logger.addHandler(
    logstash.LogstashHandler(os.getenv("LOGSTASH_HOST", "localhost"), 5959, version=1)
)

import signal
from server import ControlServer

CONTROL_SOCKET_PATH = "/var/run/proxy/control.sock"

if __name__ == "__main__":
    logger.info("Starting daemon...")

    control_server = ControlServer(CONTROL_SOCKET_PATH)
    control_server.start()
    logger.info("Ready")

    try:
        signal.pause()
    except KeyboardInterrupt:
        logger.info("Stopping...")
        control_server.stop()
        control_server.join()
