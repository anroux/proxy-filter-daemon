#!/usr/bin/env python3
from mongoengine import *


class Whitelist(Document):
    domain = StringField(required=True, unique=True)
    requester = StringField(required=True)
    comment = StringField()


# class Configuration(DynamicDocument):
#    name = StringField(required=True, unique=True)


class SourceList(Document):
    name = StringField(required=True, unique=True)
    uri = StringField(required=True)
    description = StringField()


class Domain(Document):
    name = StringField(required=True, unique=True)
    tags = ListField(StringField(max_length=50))
    comment = StringField()


class TagGroup(Document):
    name = StringField(required=True, unique=True)
    tags = ListField(StringField(max_length=50))


class BannedClients(Document):
    ip = StringField(required=True, unique=True)
    description = StringField()
