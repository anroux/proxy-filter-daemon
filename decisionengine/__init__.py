#!/usr/bin/env python3
from metadatas import TagGroup

def_forbidden_tags = [
    "porn",
    "adult",
    "aggressive",
    "agressif",
    "violence",
    "dangerous_material",
    "drogue",
    "phishing",
    "malware",
    "spyware",
    "hacking",
    "cryptojacking",
    "esrf-blacklist",
    "opendns-blacklist",
]

def_bypass_tags = ["esrf-whitelist"]
def_uncertain_tags = ["ip-uncertain"]


class DecisionEngine(object):
    @classmethod
    def get_or_def(cls, name, default):
        tags = TagGroup.objects(name=name).first()
        if tags is None:
            tags = TagGroup(name=name)
            tags.tags = default
            tags.save()

        return tags.tags

    def __init__(self):
        self.forbidden_tags = self.get_or_def("forbidden", def_forbidden_tags)
        self.bypass_tags = self.get_or_def("bypass", def_bypass_tags)
        self.uncertain_tags = self.get_or_def("uncertain", def_uncertain_tags)

    def decide(self, tags, source):
        positive = list(filter(lambda x: x in self.bypass_tags, tags))
        negative = list(filter(lambda x: x in self.forbidden_tags, tags))

        allowed = len(positive) > 0 or len(negative) == 0

        return {
            "allowed": allowed,
            "negative_tags": negative,
            "positive_tags": positive,
        }
