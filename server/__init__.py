#!/usr/bin/env python3
import logging
import socket
import sys
import os
import threading
import multiprocessing
import json

from pymongo.read_preferences import ReadPreference
import mongoengine

from .handlers.tag import TagHandler
from .handlers.comment import CommentHandler
from .handlers.taggroup import TagGroupHandler
from .handlers.sourcelist import SourceListHandler
from .handlers.allowance import AllowanceHandler
from decisionengine import DecisionEngine

logger = logging.getLogger(__name__)


class ControlServer(threading.Thread):
    def __init__(self, socket_path):
        threading.Thread.__init__(self)

        self.running = False
        self.clients = []

        logger.debug("Trying to clean existing socket (%s)", socket_path)
        try:
            os.unlink(socket_path)
        except OSError:
            if os.path.exists(socket_path):
                raise

        self.sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)

        logger.info("Bind to %s", socket_path)
        try:
            self.sock.bind(socket_path)
            os.chmod(socket_path, 0o666)
        except Exception as ex:
            logger.error("Failed to bind: %s", str(ex))

    def run(self):
        self.running = True

        logger.info("Listening for new connections")
        self.sock.listen(5)
        while self.running:
            try:
                connection, client_address = self.sock.accept()
                logger.debug("Got a new control connection")

                client = ControlClient(connection)
                logger.debug("New process created %r", client)
                self.clients.append(client)

                client.start()
            except Exception as ex:
                if self.running:
                    logger.exception(ex)

    def stop(self):
        self.running = False

        for client in self.clients:
            logger.debug("Shutting down process %r", client)
            client.stop()
            client.terminate()
            client.join()

        self.sock.shutdown(socket.SHUT_RDWR)
        self.sock.close()


class ControlClient(multiprocessing.Process):
    def __init__(self, connection):
        super().__init__()
        self.connection = connection

    def run(self):
        mongoengine.connect(
            "proxy",
            host=os.getenv("MONGO_DB", "mongodb://database/"),
            read_preference=ReadPreference.NEAREST,
        )

        self.handlers = {
            "tag": TagHandler(),
            "comment": CommentHandler(),
            "tag_group": TagGroupHandler(),
            "source": SourceListHandler(),
            "request_allowance": AllowanceHandler(DecisionEngine()),
        }

        try:
            for line in self.connection.makefile():
                # logger.debug('[{}] Request: {}'.format(self.pid, line.strip()))
                resp = {"error": "Not parsed"}
                try:
                    verb, resource, request = (line.strip().split(" ", 2) + [None])[:3]
                    handler = self.handlers.get(resource, None)
                    if handler is None:
                        raise Exception("Unsupported resource: {}".format(resource))

                    method_name = "verb_" + verb.lower()
                    if not hasattr(handler, method_name):
                        raise Exception("Unsupported verb: {}".format(verb))

                    resp = getattr(handler, method_name)(request)

                except Exception as ex:
                    logger.error(
                        "Error [{}]: {}/{}".format(line.strip(), type(ex).__name__, ex)
                    )
                    resp = {"error": str(ex), "error_type": type(ex).__name__}

                finally:
                    try:
                        self.connection.sendall(
                            (json.dumps(resp) + "\n").encode("utf-8")
                        )
                    except Exception as sendex:
                        logger.error(
                            "Unable to answer to [{}]: {}/{}".format(
                                line.strip(), type(sendex).__name__, sendex
                            )
                        )
                        logger.error("The missed message was {}".format(resp))
                        raise
        finally:
            self.connection.close()

    def stop(self):
        self.connection.shutdown(socket.SHUT_RDWR)
        self.connection.close()
