#!/usr/bin/env python3
from database import Database


class TagHandler(object):
    def __init__(self):
        self.db = Database()

    def verb_get(self, request):
        domain, opt = (request.rsplit(":", 1) + [None])[:2]

        if opt == "walk":
            return self.db.get_domain_or_ip(domain)
        else:
            return self.db.get_fulldomain(domain)

    def verb_add(self, request):
        domain, _tags = request.split(" ", 1)
        tags = _tags.split(" ")
        self.db.apply_tags(domain, tags)

    def verb_remove(self, request):
        domain, _tags = request.split(" ", 1)
        tags = _tags.split(" ")
        self.db.remove_tags(domain, tags)
