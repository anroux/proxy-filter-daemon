#!/usr/bin/env python3
import json
from database import Database


class CommentHandler(object):
    def __init__(self):
        self.db = Database()

    def verb_get(self, request):
        domain = request
        return self.db.get_comment(domain)

    def verb_set(self, request):
        domain, _comment = request.split(" ", 1)
        comment = json.loads(_comment)
        self.db.set_comment(domain, comment)
