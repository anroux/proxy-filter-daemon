#!/usr/bin/env python3
import logging
import io
import os.path
import tarfile
import shutil
import urllib.request

from database import Database
from metadatas import SourceList

logger = logging.getLogger(__name__)


class SourceListHandler(object):
    def __init__(self):
        self.db = Database()

    def verb_add(self, request):
        new_list = SourceList()
        new_list.name = "test"
        new_list.uri = "test"
        new_list.save()

    def verb_list(self, request):
        result = []

        sources = SourceList.objects()
        for source in sources:
            result.append(
                {
                    "name": source.name,
                    "description": source.description,
                    "uri": source.uri,
                }
            )

        return {"sourcelists": result}

    def verb_import(self, request):
        name = request
        sourcelist = next(SourceList.objects(name=name))
        url = sourcelist.uri

        logger.info("Starting to import {}...".format(url))

        domains = {}
        with io.BytesIO() as f:
            logger.info("Downloading {}...".format(url))
            with urllib.request.urlopen(url) as r:
                shutil.copyfileobj(r, f)
            f.seek(0)

            logger.info("Uncompressing {}...".format(name))
            with tarfile.open(fileobj=f, mode="r:gz") as tar:
                for tarinfo in tar:
                    if not tarinfo.isfile():
                        continue

                    if os.path.basename(tarinfo.name) == "domains":
                        tag = os.path.basename(os.path.dirname(tarinfo.name))
                        tf = tar.extractfile(tarinfo)

                        for line in tf:
                            domain = line.decode("utf-8").rstrip()
                            if len(domain) == 0:
                                continue

                            if domain not in domains:
                                domains[domain] = []

                            domains[domain].append(tag)

        logger.info("Merging into DB...")
        self.db.apply_tags_multiple(domains)

        logger.info("Finished")
        return {"imported": len(domains)}
