#!/usr/bin/env python3
import logging
import json
import time

from database import Database
from ua_parser import user_agent_parser

logger = logging.getLogger(__name__)


class AllowanceHandler(object):
    def __init__(self, decisionengine):
        self.db = Database()
        self.decisionengine = decisionengine

    def verb_get(self, request):
        start_time = time.time()

        domain, path, _info = request.split(" ", 2)
        info = json.loads(_info)

        tags = []
        str_decision = "OK"

        source = info.get("source", "unknown")
        useragent = info.get("useragent", "unknown")

        log_ua = "Unknown"
        if useragent != "-":
            parsed_ua = user_agent_parser.Parse(useragent)
            log_ua = "{}/{}".format(
                parsed_ua["os"]["family"], parsed_ua["user_agent"]["family"]
            )

        is_banned = self.db.is_client_banned(source)

        if not is_banned:
            tags = self.db.get_domain_or_ip(domain)
            decision = self.decisionengine.decide(tags, source)
            str_decision = "OK" if decision["allowed"] else "BLKD"

        else:
            str_decision = "BNED"
            decision = {"allowed": False, "banned": True}

        elapsed_time = time.time() - start_time

        logger.info(
            "{: <4} {: >15} {: <20} {: <50} {}".format(
                str_decision, source, log_ua, ",".join(tags), domain
            ),
            extra={
                "decision": str_decision,
                "source": source,
                "user-agent": useragent,
                "domain": domain,
                "tags": tags,
                "decision_duration": elapsed_time,
            },
        )

        return decision
