#!/usr/bin/env python3
from metadatas import TagGroup


class TagGroupHandler(object):
    def verb_get(self, request):
        group_name = request
        return TagGroup.objects(name=group_name).first().tags
